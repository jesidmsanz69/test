import React from 'react';
import { connect } from 'react-redux';
import { loginRequest, logoutRequest } from '../redux/actions/loginActions';
import utilities from '../utils/utilities';

// import '../assets/styles/components/Login.scss';
import logo from '../assets/static/logo.jpg';
import { message } from '../utils/notification';
import '../assets/styles/components/Login.scss';

class Login extends React.Component {
  constructor() {
    super();
    this.state = {
      form: {
        typeId: '',
        username: '',
        password: '',
        rememberme: null,
      },
    };

    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleInput = this.handleInput.bind(this);
  }

  handleSubmit(event) {
    event.preventDefault();
    const { form } = this.state;
    const { returnUrl } = utilities.getQuery(this.props.location);
    const redirecUrl = this.props.history.location.state
      ? this.props.history.location.state.from.pathname
      : returnUrl || '/admin';
    this.props.loginRequest(form, redirecUrl);
  }

  handleInput(event) {
    console.log('handleInput');
    const { form } = this.state;
    this.setState({
      form: {
        ...form,
        [event.target.name]: event.target.value,
      },
    });
  }

  componentDidMount() {
    // reset login status
    this.props.logoutRequest(false);
  }

  componentDidUpdate(prevProps) {
    if (prevProps.login.hasError !== this.props.login.hasError && this.props.login.hasError) {
      this.props.login.hasError = false;
      message(this.props.login.error, 3);
    }
  }

  render() {
    return (
      <section className="top-container-full-width">
        <div className="top-container">
          <a href="/" className="mb-3">
            <img src={logo} alt="Logo" />
          </a>
          <div className="login-container">
            <form className="login-form" onSubmit={this.handleSubmit}>
              <input
                name="username"
                className="log"
                type="text"
                placeholder="Username"
                onChange={this.handleInput}
                value={this.state.form.username}
                required
              />

              <input
                name="password"
                className="log"
                type="password"
                placeholder="Password"
                onChange={this.handleInput}
                value={this.state.form.password}
                required
              />

              <input className="top-btn top-btn-primary mb-3" type="submit" value="LOGIN" />
            </form>
          </div>
        </div>
      </section>
    );
  }
}
const mapStateToProps = ({ login, from }) => ({ login, from });

export default connect(mapStateToProps, { loginRequest, logoutRequest })(Login);
