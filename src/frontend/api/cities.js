const axios = require('axios');
const { getAxiosError } = require('../utils/utilities');

const mainRoute = 'cities';

const list = async (initialData) => {
  try {
    const { data } = await axios.get(mainRoute, initialData);
    return data.body;
  } catch (error) {
    return { error: true, message: getAxiosError(error) };
  }
};

const listActive = async (initialData) => {
  try {
    const { data } = await axios.get(`${mainRoute}/active`, initialData);
    return data.body;
  } catch (error) {
    return { error: true, message: getAxiosError(error) };
  }
};

const detail = async (id, initialData) => {
  try {
    const { data } = await axios.get(`${mainRoute}/${id}`, initialData);
    return data.body;
  } catch (error) {
    return { error: true, message: getAxiosError(error) };
  }
};

const create = async (initialData) => {
  try {
    const { data } = await axios.post(mainRoute, initialData);
    return data.body;
  } catch (error) {
    return { error: true, message: getAxiosError(error) };
  }
};

const update = async (id, initialData) => {
  try {
    const { data } = await axios.put(`${mainRoute}/${id}`, initialData);
    return data.body;
  } catch (error) {
    return { error: true, message: getAxiosError(error) };
  }
};

const remove = async (id) => {
  try {
    const { data } = await axios.delete(`${mainRoute}/${id}`);
    return data.body;
  } catch (error) {
    console.log('error', error);
    return { error: true, message: getAxiosError(error) };
  }
};

module.exports = {
  list,
  listActive,
  detail,
  create,
  update,
  remove,
};
