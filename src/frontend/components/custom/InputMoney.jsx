import React, { useState } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

// import { FormControl } from 'react-bootstrap';
import CurrencyFormat from 'react-currency-format';

function InputMoney(props) {
  const [value, setValue] = useState({ formattedValue: '', value: '' });
  const handleChange = (values) => {
    console.log('handleChange values', values);
    setValue(values);
    props.onValueChange(values.value);
  };
  return (
    <CurrencyFormat
      {...props}
      className={classNames('form-control', props.className)}
      // customInput={(props) => <FormControl {...props} />}
      thousandSeparator={true}
      value={value.formattedValue}
      onValueChange={(values) => handleChange(values)}
    />
  );
}

InputMoney.propTypes = {
  id: PropTypes.string,
  name: PropTypes.string,
  className: PropTypes.string,
  value: PropTypes.string,
  onChange: PropTypes.func,
};

export default InputMoney;
