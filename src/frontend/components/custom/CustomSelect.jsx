import React, { useState } from 'react';
import PropTypes from 'prop-types';
import Select from 'react-dropdown-select';
import classNames from 'classnames';
import styled from 'styled-components';

const SelectContainer = styled(Select)`
  .react-dropdown-select-type-single {
    flex-wrap: nowrap;
    span {
      white-space: nowrap;
    }
    .react-dropdown-select-input {
      width: 100%;
    }
  }
`;

function CustomSelect(props) {
  // console.log('CustomSelect props', props);
  const [invalid, setInvalid] = useState(props.required || false);

  let selectedValues = props.values;
  // console.log('props.values[0]', props.values[0]);
  if (
    props.values &&
    props.values.length &&
    (!props.values[0] || !props.values[0][props.valueField])
  ) {
    selectedValues =
      props.options && props.options.length
        ? props.options.filter((item) => props.values.includes(item[props.valueField])) || []
        : [];
  }
  let selectRef;
  const handleChange = (row) => {
    props.onChange(row);
    if (selectRef && props.required) {
      const inputContainer = selectRef.getSelectRef();
      const input = inputContainer.querySelector('.react-dropdown-select > input');
      input.checkValidity();
      const isInvalid = !!inputContainer.querySelector('input:invalid');
      setInvalid(isInvalid);
    }
  };

  return (
    <SelectContainer
      searchable={true}
      {...props}
      onChange={handleChange}
      ref={(el) => {
        selectRef = el;
      }}
      values={selectedValues}
      className={classNames(props.className, { invalid })}
    />
  );
}

CustomSelect.propTypes = {
  onChange: PropTypes.func,
};

export default CustomSelect;
