import React from 'react';
import PropTypes from 'prop-types';
import Dropzone from 'react-dropzone-uploader';
import 'react-dropzone-uploader/dist/styles.css';
import classNames from 'classnames';
// import styled from 'styled-components';

import '../../assets/styles/components/custom/UploadFile.scss';

function UploadFile(props) {
  // specify upload params and url for your files
  const getUploadParams = ({ meta }) => {
    return { url: props.url || '' };
  };

  // called every time a file's `status` changes
  const handleChangeStatus = ({ meta, file }, status) => {
    // console.log('handleChangeStatus');
    console.log('status', status);
    // console.log('meta', meta);
    // console.log('file', file);
    if (status === 'ready') {
      console.log('file', file);
      props.onAddFile && props.onAddFile(file);
    } else if (status === 'removed') {
      props.onRemoveFile && props.onRemoveFile(file);
    }
  };

  // const Container = styled.div`
  //   display: flex;
  //   &.no-auto-upload {
  //     .dzu-previewStatusContainer {
  //       progress {
  //         display: none;
  //       }
  //       span:last-child {
  //         display: none;
  //       }
  //     }
  //   }
  // `;

  // receives array of files that are done uploading when submit button is clicked
  // const handleSubmit = (files, allFiles) => {
  //   console.log(files.map((f) => f.meta));
  //   allFiles.forEach((f) => f.remove());
  // };

  return (
    <div className={classNames('container', { 'no-auto-upload': !props.autoUpload })}>
      <Dropzone
        autoUpload={!!props.autoUpload}
        {...props}
        getUploadParams={getUploadParams}
        onChangeStatus={handleChangeStatus}
        accept={props.accept || 'image/*'}
        inputContent={props.text}
        submitButtonContent="Finish"
        // onSubmit={props.onSubmit}
      />
    </div>
  );
}

UploadFile.propTypes = {
  url: PropTypes.string,
  text: PropTypes.string,
  accept: PropTypes.string,
  autoUpload: PropTypes.bool,
  onSubmit: PropTypes.func,
  onAddFile: PropTypes.func,
  onRemoveFile: PropTypes.func,
};

export default UploadFile;
