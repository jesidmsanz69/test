import React from 'react';

import { Col, Container, Row } from 'react-bootstrap';
import '../assets/styles/components/ListAmenities.scss';

const ListAmenities = () => {
  return (
    <>
      <section className="description-amenities">
        <Container>
          <Row className="justify-content-between">
            <Col sm={5}>
              <h2 className="h2">Community Amenities</h2>
              <ul className="amenities-list-1">
                <li>Roof deck, lounge seating and beautiful ocean views</li>
                <li>Lobby Lounge</li>
                <li>Parcel Locker System</li>
                <li>Vehicle charging stations</li>
                <li>Wi-Fi in Lobby Lounge</li>
                <li>Controlled Access Entry</li>
                <li>Subterranean Assigned Parking (Single Spots one per Bedroom) *</li>
                <li>Direct Elevator Access from Garage</li>
                <li>Professional On-Site Management</li>
                <li>Easy Access to 405 Fwy, 10 Fwy and 90 Fwy</li>
                <li>Walking Distance to Shopping, Restaurants in Brentwood Village and Sawtelle</li>
                <li>Future Metro Stop Just east of Federal on Wilshire</li>
              </ul>
            </Col>
            <Col sm={5}>
              <p className="h2">Apartment Amenities</p>
              <ul className="amenities-list ">
                <li>Keyless Entry MIWA Locks</li>
                <li>High Ceilings</li>
                <li>Walk In Custom Closets</li>
                <li>Front Loading Washer/Dryer</li>
                <li>Stainless Steel Appliances</li>
                <li>Walk in Shower &amp; Large Soaking Tubs*</li>
                <li>Movable Kitchen Islands *</li>
                <li>Large Balconies and/or Garden Patio *</li>
                <li>Central A/C and Heating</li>
                <li>Gas Ranges</li>
                <li>Wood Plank Style Flooring</li>
                <li>Luxury High End Carpet (Bedroom)</li>
                <li>Control Access Entry</li>
                <li>Quartz Countertops</li>
              </ul>
            </Col>
          </Row>
          <p className="text-center">* Some Units</p>
        </Container>
      </section>
    </>
  );
};

export default ListAmenities;
