import React from 'react';

import '../assets/styles/components/Template.scss';

export default function PublicLayout(props) {
  const { isLogged, children, location } = props;
  return <>{children}</>;
}
