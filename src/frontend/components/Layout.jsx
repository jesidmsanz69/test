import React from 'react';

// import Header from './Header';
// import Footer from './Footer';

// import AdminLayout from './admin/AdminLayout';
// import PublicLayout from './PublicLayout';

import loadable from '@loadable/component';

const AdminLayout = loadable(() => import('./admin/AdminLayout'));
const PublicLayout = loadable(() => import('./PublicLayout'));

// import QuoteBottom from './quote-bottom';
// import AdminHeader from './admin/AdminHeader';
// import AdminFooter from './admin/AdminFooter';
// import AdminMenu from './admin/AdminMenu';

// const Layout = ({ isLogged, children, location }) => {
//   const isLogin = !isLogged || location.indexOf('/login') >= 0;
//   // console.log('isLogin', isLogin);
//   return (
//     <>
//       {isLogin && <Header location={location} />}

//       {children}

//       {isLogin && <QuoteBottom />}

//       {isLogin && <Footer />}
//     </>
//   );
// };

//Inicio
// const publicLayout = (children, location, isLogged) => (
//   <>
//     <Header location={location} isLogged={isLogged} />

//     {children}

//     <Footer isLogged={isLogged} />
//   </>
// );
//Fin

// const adminLayout = (children, location, isLogged) => (
//   <div className="admin-layout">
//     <AdminMenu />
//     <div className="admin-layout__right">
//       <AdminHeader location={location} isLogged={isLogged} />
//       <div className="content">{children}</div>

//       <AdminFooter isLogged={isLogged} />
//     </div>
//   </div>
// );

const Layout = (props) => {
  const { isLogged, children, location } = props;
  const isLogin = location.indexOf('/login') >= 0;
  const isAdmin = location.indexOf('/admin') >= 0;
  // console.log('isAdmin', isAdmin);
  if (isAdmin && isLogged) {
    return (
      <AdminLayout location={location} isLogged={isLogged}>
        {children}
      </AdminLayout>
    );
  }
  if (isLogin || (isAdmin && !isLogged)) {
    return <>{children}</>;
  }

  return (
    <PublicLayout location={location} isLogged={isLogged}>
      {children}
    </PublicLayout>
  );
  // return publicLayout(children, location, isLogged);
};

export default Layout;
