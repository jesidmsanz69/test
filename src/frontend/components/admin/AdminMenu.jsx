import React, { useState } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {
  faEnvelope,
  faHandPointLeft,
  faHandPointRight,
  faTimes,
} from '@fortawesome/free-solid-svg-icons';

import { NavLink } from 'react-router-dom';
import { connect } from 'react-redux';
import logo from '../../assets/static/logo-admin.png';

import '../../assets/styles/components/admin/AdminMenu.scss';

function AdminMenu(props) {
  const [disableToggle, setDisableToggle] = useState(false);
  const toggleButtonCompactMenu = (event) => {
    setDisableToggle(true);
    // setDisableToggle(false);
    setTimeout(() => setDisableToggle(false), 450);
    props.toggleCompactMenu();
  };
  return (
    <div className="menu-left">
      <div className="menu-left__header">
        {/* <img
          alt="Logo"
          src={logo}
          width="40"
          height="40"
          className="d-inline-block align-top rounded-circle text-white"
        />{' '} */}
        <div className="navbar-brand-logo">
          <img src={logo} alt="Logo admin" />
        </div>
        <span className="user-text">{props.login.user.firstName}</span>
        <button
          type="button"
          onClick={toggleButtonCompactMenu}
          className={`btn text-white rounded-circle btn-toggle-compact-menu btn-primary${
            disableToggle ? ' no-events' : ''
          }`}
        >
          {props.compactMenu && <FontAwesomeIcon icon={faHandPointRight} size="lg" />}
          {!props.compactMenu && <FontAwesomeIcon icon={faHandPointLeft} size="lg" />}
        </button>
        <button
          type="button"
          onClick={props.toggleOpenMenu}
          className="btn text-white ml-auto rounded-circle btn-toggle-compact-menu-mobile btn-primary"
        >
          <FontAwesomeIcon icon={faTimes} size="lg" />
        </button>
      </div>
      <div className="menu-left__content">
        <ul>
          <li>
            <NavLink to="/admin/contacts" activeClassName="active" className="nav-link">
              <FontAwesomeIcon icon={faEnvelope} />
              <span>Contacts</span>
            </NavLink>
          </li>
        </ul>
      </div>
    </div>
  );
}

const mapStateToProps = ({ login }) => ({ login });

const mapDispatchToProps = {
  //   logoutRequest,
};

export default connect(mapStateToProps, mapDispatchToProps)(AdminMenu);
