import React, { Component } from 'react';
import { connect } from 'react-redux';
// import classNames from 'classnames';
import { Navbar, Nav } from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faBars, faTimes } from '@fortawesome/free-solid-svg-icons';

// import gravatar from '../../utils/gravatar';
import { logoutRequest } from '../../redux/actions/loginActions';
// import { userIsInRole } from '../../utils/auth';
// import logo from '../../assets/static/logo-admin.png';
import '../../assets/styles/components/admin/AdminHeader.scss';

class AdminHeader extends Component {
  constructor(props) {
    super(props);
    this.handleLogout = this.handleLogout.bind(this);
  }

  handleLogout() {
    this.props.logoutRequest();
  }

  render() {
    return (
      <Navbar bg="light" variant="light" className="mb-4">
        {/* <Navbar.Brand href="#home">
          <img
            alt="Logo"
            src={logo}
            width="40"
            height="40"
            className="d-inline-block align-top rounded-circle"
          />{' '}
          Admin
        </Navbar.Brand> */}
        <button
          type="button"
          onClick={this.props.toggleOpenMenu}
          className="btn text-primary btn-toggle-compact-menu-mobile"
        >
          {this.props.openMenu && <FontAwesomeIcon icon={faTimes} size="lg" />}
          {!this.props.openMenu && <FontAwesomeIcon icon={faBars} size="lg" />}
        </button>

        <div className="mr-auto" />
        {/* {this.props.login.user.firstName} */}
        <Nav>
          <Nav.Link href="#logout" className="btnlogout" onClick={this.handleLogout}>
            LOGOUT
          </Nav.Link>
        </Nav>
      </Navbar>
    );
  }
}

const mapStateToProps = ({ login }) => ({ login });

const mapDispatchToProps = {
  logoutRequest,
};

export default connect(mapStateToProps, mapDispatchToProps)(AdminHeader);
