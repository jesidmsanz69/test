import React from 'react';
// import { BrowserRouter } from 'react-router-dom';
import { renderRoutes } from 'react-router-config';
// import loadable from '@loadable/component';
// import serverRoutes from './serverRoutes';
// import PrivateRoute from './PrivateRoute';
import Routes from './serverRoutes';
import Layout from '../components/Layout';
// import { loadReCaptcha } from 'react-recaptcha-v3';
// const Layout = loadable(() => import('../components/Layout'));
// const AdminLayout = loadable(() => import('../components/admin/AdminLayout'));
// import utilities from '../utils/utilities';
// import ShippingDetails from '../containers/shippings/ShippingDetails';
// import Annular from '../containers/shippings/Annular';
// // import Search from '../containers/shippings/Search';
// import Manifiest from '../containers/shippings/Manifiest';
// import RangePrint from '../containers/shippings/RangePrint';

const locationUrl = window.location.pathname;

const App = ({ isLogged }) => {
  // const routes = serverRoutes(isLogged);
  // const adminRoutes = routes.filter((item) => item.layout === 'admin');
  // const publicRoutes = routes.filter((item) => !item.layout);
  // const noLayoutRoutes = routes.filter((item) => item.layout === 'none');

  // const adminRoutesPaths = adminRoutes.map((item) => item.path);
  // const publicRoutesPaths = publicRoutes.map((item) => item.path);
  // const noLayoutRoutesPaths = noLayoutRoutes.map((item) => item.path);
  // console.log('adminRoutesPaths', adminRoutesPaths);

  return (
    <>
      {/* <>
        <Route path={adminRoutesPaths}>
          <AdminLayout isLogged={isLogged} location={locationUrl}>
            {adminRoutes.map((item) => (
              <PrivateRoute key={item.id} {...item} />
            ))}
          </AdminLayout>
        </Route>
        <Route path={publicRoutesPaths}>
          <Layout isLogged={isLogged} location={locationUrl}>
            {publicRoutes.map((item) => (
              <Route key={item.id} {...item} />
            ))}
          </Layout>
        </Route>
        <Route path={noLayoutRoutesPaths}>
          {noLayoutRoutes.map((item) => (
            <Route key={item.id} {...item} />
          ))}
        </Route>
      </> */}
      <Layout isLogged={isLogged} location={locationUrl}>
        {renderRoutes(Routes(isLogged, false))}
      </Layout>
    </>
  );
};

export default App;
// export default class App extends Component {
//   componentDidMount() {
//     loadReCaptcha(utilities.recaptcha.PUBLIC_KEY, (res) => console.log('loadReCaptcha', res));
//   }

//   render() {
//     const { isLogged } = this.props;
//     return (
//       <BrowserRouter>
//         <Layout isLogged={isLogged} location={locationUrl}>
//           {renderRoutes(Routes(isLogged))}
//         </Layout>
//       </BrowserRouter>
//     );
//   }
// }
