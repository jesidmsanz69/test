import axios from 'axios';
import { token } from './auth';

axios.defaults.baseURL = '/api/';
axios.defaults.headers.common = { Authorization: `bearer ${token}` };
export default axios;
