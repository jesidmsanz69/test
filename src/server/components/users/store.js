'use strict';

const { Op } = require('sequelize');

module.exports = function setupUser(UserModel, RoleModel, RoleUserModel) {
  function findById(id) {
    return UserModel.findByPk(id);
  }

  async function exists(user) {
    const cond = {
      where: {
        [Op.or]: [
          { uuid: user.uuid },
          { username: user.username },
          // { email: user.email },
          // { documentTypeId: user.documentTypeId, idNumber: user.idNumber },
        ],
      },
    };
    const exists = await UserModel.findOne(cond);
    return exists;
  }

  async function addToRole(user, roleId) {
    const role = await RoleUserModel.create({ userId: user.id, roleId });
    return role;
  }

  async function getRoles({ id, username }) {
    const cond = {
      where: {
        [Op.or]: [{ id: id || 0 }, { username: username || '' }],
      },
      include: 'roles',
      raw: true,
    };
    const user = await UserModel.findAll(cond);
    // console.log('getRoles user', user);
    const roles = user.reduce((rolesList, item) => {
      rolesList.push(item['roles.name']);
      return rolesList;
    }, []);
    // console.log('getRoles roles', roles);
    return roles;
  }

  async function createOrUpdate(user) {
    const existingUser = await exists(user);
    if (existingUser) {
      const updated = await UserModel.updated(user, cond);
      return updated ? UserModel.findOne(cond) : existingUser;
    }

    const result = await UserModel.create(user);
    return result.toJSON();
  }

  async function create(user, roleId) {
    const existingUser = await exists(user);
    if (!existingUser) {
      const result = await UserModel.create(user);
      if (roleId) await addToRole(result, roleId);
      return result.toJSON();
    }
    return existingUser;
  }

  function findByUuid(uuid) {
    return UserModel.findOne({
      where: {
        uuid,
      },
    });
  }

  function findAll() {
    return UserModel.findAll();
  }

  function findByUsername(username) {
    return UserModel.findOne({
      where: {
        username,
      },
      raw: true,
    });
  }

  async function changePassword({ id, password }) {
    const result = await UserModel.update(
      { password },
      {
        where: {
          id,
        },
      }
    );
    return result;
  }

  function statusChange({ id, statusUserId, updatedBy }) {
    const cond = {
      where: {
        id,
      },
    };
    return UserModel.update({ statusUserId, updatedBy }, cond);
  }

  return {
    findById,
    exists,
    createOrUpdate,
    create,
    findByUuid,
    findAll,
    findByUsername,
    addToRole,
    getRoles,
    changePassword,
    statusChange,
  };
};
