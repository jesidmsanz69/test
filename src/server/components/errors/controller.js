const db = require('../../db/index.js');
const utilities = require('../../utils/utilities.js');

function create(req, res, ex) {
  console.log('create error');
  return new Promise(async (resolve, reject) => {
    try {
      const ipAddress = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
      const page = req.url;
      const message = `Exception generated on ${new Date()}
      <br /><br /> Page location: ${page}
      <br /><br /> Message: ${ex.message || ''}
      <br /><br /> Stack Trace: <br /><br />${ex.stack || ''}
      <br /><br /> Base Exception: <br /><br />${ex}
      <br /><br /> UrlReferrer:${req.headers.referer}
      <br /><br /> Browser:${req.get('user-agent')}
      <br /><br /> Page:${page}
      <br /><br /> IP:${ipAddress}`;
      const website = utilities.NAME_COMPANY;
      const websiteurl = utilities.DOMAIN_WEBSITE;
      const status = 'Unread';
      const ipserver = utilities.IP_SERVER;

      const model = {
        message,
        website,
        websiteurl,
        ipserver,
        status,
        page,
      };

      // console.log('[db]', db);
      const { Errors } = await db(2);
      const result = await Errors.create(model);
      resolve(result);
    } catch (err) {
      console.log('Error to save APP', err);
      // reject(err);
    }
  });
}

module.exports = {
  create,
};
