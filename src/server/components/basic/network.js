import passport from 'passport';

const express = require('express');
const controller = require('./controller');
const response = require('../../network/response');

const router = express.Router();

// GET: api/basics
router.get('/', passport.authenticate('jwt', { session: false }), async function(req, res) {
  try {
    const result = await controller.findAll();
    response.success(req, res, result);
  } catch (error) {
    console.log('ERROR: ', error);
    response.error(req, res, 'Error on basics', 400, error);
  }
});

// GET: api/basics/active
router.get('/active', async function(req, res) {
  try {
    const result = await controller.findAllActive();
    response.success(req, res, result);
  } catch (error) {
    console.log('ERROR: ', error);
    response.error(req, res, 'Error on basics', 400, error);
  }
});

// GET: api/basics/1
router.get('/:id', passport.authenticate('jwt', { session: false }), async function(req, res) {
  try {
    const result = await controller.findById(req.params.id);
    response.success(req, res, result);
  } catch (error) {
    console.log('ERROR: ', error);
    response.error(req, res, 'Error on basics', 400, error);
  }
});

// POST: api/basics
router.post('/', async function(req, res) {
  try {
    const result = await controller.create(req.body);
    response.success(req, res, result);
  } catch (error) {
    console.log('ERROR: ', error);
    response.error(req, res, 'Error on basics', 400, error);
  }
});

// POST: api/basics/1
router.put('/:id', async function(req, res) {
  try {
    const result = await controller.update(req.params.id, req.body);
    response.success(req, res, result);
  } catch (error) {
    console.log('ERROR: ', error);
    response.error(req, res, 'Error on basics', 400, error);
  }
});

// DELETE: api/basics
router.delete('/:id', async function(req, res) {
  try {
    console.log('delete: basics');
    const model = await controller.deleteById(req.params.id);
    response.success(req, res, model);
  } catch (error) {
    console.log('ERROR: ', error);
    response.error(req, res, 'Error on basics', 400, error);
  }
});

module.exports = router;
