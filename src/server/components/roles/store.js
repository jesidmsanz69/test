'use strict';

module.exports = function setupModel(RoleModel) {
  function findById(id) {
    return RoleModel.findByPk(id);
  }

  async function findByName(name) {
    const cond = {
      where: {
        name,
      },
    };
    const role = await RoleModel.findOne(cond);
    return role;
  }

  async function exists(role) {
    const cond = {
      where: {
        name: role.name,
      },
    };
    const exists = await RoleModel.findOne(cond);
    return exists;
  }

  async function update(role) {
    const existingRole = await exists(role);
    if (existingRole) {
      const updated = await RoleModel.updated(role, cond);
      return updated ? RoleModel.findOne(cond) : existingRole;
    }

    return null;
  }

  async function create(role) {
    try {
      const existingRole = await exists(role);
      if (!existingRole) {
        const result = await RoleModel.create(role);
        return result.toJSON();
      }
    } catch (error) {
      console.log(error);
    }

    return null;
  }

  function findAll() {
    return RoleModel.findAll({ order: [['name', 'ASC']] });
  }

  return {
    findById,
    update,
    create,
    findAll,
    exists,
    findByName,
  };
};
