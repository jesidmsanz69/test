/* eslint-disable no-param-reassign */
/* eslint-disable space-before-function-paren */
const DataTypes = require('sequelize');
const setupDatabase = require('../../db/lib/db');
//
// Creating our User model
//Set it as export because we will need it required on the server
module.exports = function(config) {
  const sequelize = setupDatabase(config);
  const Roles = sequelize.define('roles', {
    name: {
      allowNull: false,
      type: DataTypes.STRING,
      validate: {
        notEmpty: true,
      },
    },
    description: {
      allowNull: false,
      type: DataTypes.STRING,
      defaultValue: 'no description',
    },
  });

  // Roles.associate = (models) => {
  //   Roles.belongsToMany(models.Users, {
  //     through: 'RoleUsers',
  //     as: 'users',
  //     foreignKey: 'roleId',
  //   });
  // };

  return Roles;
};
