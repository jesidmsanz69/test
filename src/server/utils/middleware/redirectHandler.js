const redirects = require('../../network/redirects');

function redirectHandler(req, res, next) {
  //   console.log('req.url', req.url);
  const redirect = redirects.find((item) => item.from === req.url || `${item.from}/` === req.url);
  if (redirect && redirect.to) {
    res.redirect(redirect.to);
  } else {
    next();
  }
}

module.exports = redirectHandler;
