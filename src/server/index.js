require('ignore-styles');
// require('@babel/polyfill');
require('core-js/stable');
require('regenerator-runtime/runtime');

require('@babel/register')({
  ignore: [/(node_modules)/],
  presets: ['@babel/preset-env', '@babel/preset-react'],
});

require('asset-require-hook')({
  extensions: ['jpg', 'png', 'gif', 'svg'],
  // name: '/assets/[hash].[ext]',
  name: '/assets/[hash]/[name].[ext]',
});

require('./server.js');
