/* eslint-disable indent */
// import getManifest from '../getManifest';
// import { ChunkExtractor } from '@loadable/server';

// const files = false;
// if (process.env.NODE_ENV !== 'development') files = getManifest();
const render = (html, preloadedState, req, extractor, helmet) => {
  // You can now collect your script tags
  const scriptTags = extractor.getScriptTags(); // or extractor.getScriptElements();
  // You can also collect your "preload/prefetch" links
  const linkTags = extractor.getLinkTags(); // or extractor.getLinkElements();
  // And you can even collect your style tags (if you use "mini-css-extract-plugin")
  const styleTags = extractor.getStyleTags(); // or extractor.getStyleElements();

  return `<!DOCTYPE html>
    <html lang="en" class="no-webp" ${helmet.htmlAttributes.toString()}>
    <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="theme-color" content="#1d5469" />
    <!-- %PUBLIC_URL% -->
    <link rel="icon" href="/images/favicon.ico" />

    <link rel="preconnect" href="https://www.google-analytics.com" crossorigin />
    <link rel="dns-prefetch" href="https://www.google-analytics.com" />

    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin />
    <link rel="dns-prefetch" href="https://fonts.gstatic.com" />

    <link rel="stylesheet preload prefetch" href="https://fonts.googleapis.com/css2?family=Marcellus&family=Montserrat:wght@400;700&display=swap" as="style" type="text/css" crossorigin="anonymous">

    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.6.0/dist/leaflet.css" />
    
    <!--
      manifest.json provides metadata used when your web app is installed on a
      user's mobile device or desktop. See https://developers.google.com/web/fundamentals/web-app-manifest/
    -->
    <!-- %PUBLIC_URL% 
    <link rel="manifest" href="/manifest.json" />-->
        ${linkTags}
        ${styleTags}
        <!--<script src="/socket.io/socket.io.js"></script>-->
      ${helmet.title.toString()}
      ${helmet.meta.toString()}
      ${helmet.link.toString()}
      ${helmet.style.toString()}
      ${helmet.noscript.toString()}
    </head>
   
    <body>
        <div id="root">${html}</div>

      <!-- Global site tag (gtag.js) - Google Analytics -->
      <script async src="https://www.googletagmanager.com/gtag/js?id=G-LDYG1K18BE"></script>
      <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'G-LDYG1K18BE');
      </script>

        <script type="application/ld+json">
      {
          "@context": "http://schema.org",
          "@type": "LocalBusiness",
          "address": {
              "@type": "PostalAddress",
              "streetAddress": "1330 Federal Ave.",
              "addressLocality": "LA",
              "addressRegion": "CA",
              "postalCode": "90025"
              },
          "name": "Abstract 1330",
          "description": "Located just a short walk from the Brentwood village you will love the unique design, extra large bedrooms and relaxing lifestyle offered to residents at The Abstract.",
          "telephone": "424-369-1035",
          "url": "https://abstract1330.com",
          "image": "https://abstract1330.com/images/logo.png",
          "logo": "https://abstract1330.com/images/logo.png",
          "priceRange": "$"
      }
  </script>
        <script>
          // WARNING: See the following for security issues around embedding JSON in HTML:
          // https://redux.js.org/recipes/server-rendering/#security-considerations
          window.__PRELOADED_STATE__ = ${JSON.stringify(preloadedState).replace(/</g, '\\u003c')}
        </script>
        ${scriptTags}
        ${helmet.script.toString()}
        <script>setTimeout(function(){(function(){ var s = document.createElement('script'), e = ! document.body ? document.querySelector('head') : document.body; s.src = 'https://acsbapp.com/apps/app/dist/js/app.js'; s.async = true; s.onload = function(){ acsbJS.init({ statementLink : '', footerHtml : '', hideMobile : false, hideTrigger : false, language : 'en', position : 'right', leadColor : '#146FF8', triggerColor : '#146FF8', triggerRadius : '50%', triggerPositionX : 'right', triggerPositionY : 'bottom', triggerIcon : 'people', triggerSize : 'medium', triggerOffsetX : 20, triggerOffsetY : 20, mobile : { triggerSize : 'small', triggerPositionX : 'right', triggerPositionY : 'center', triggerOffsetX : 0, triggerOffsetY : 0, triggerRadius : '50%' } }); }; e.appendChild(s);}());}, 3000);</script>
    </body>
    </html>
  `;
};

export default render;
