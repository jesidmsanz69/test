'use strict';

const defaults = require('defaults');
// const setupDatabaseOracle = require('../lib/dbOracle');
// const setupShipping = require('../../components/shippings/store');

module.exports = async (config) => {
  // console.log('config', config);
  // eslint-disable-next-line no-param-reassign
  config = defaults(config, {
    dialect: 'sqlite',
    pool: {
      max: 10,
      min: 0,
      idle: 10000,
    },
    query: {
      raw: true,
    },
  });

  // const sequelize = setupDatabaseOracle(config);

  // await sequelize.authenticate();

  // if (config.setup) {
  //   await sequelize.sync({ force: true });
  // }

  // const ShippingsCMS = setupShipping(null, sequelize);

  // return { sequelize, ShippingsCMS };
};
