'use strict';

const setupDatabase = require('../lib/db');
const setupError = require('../../components/errors/store');

module.exports = async (config, dbId) => {
  // console.log('config', config);
  // eslint-disable-next-line no-param-reassign
  //   config = defaults(config, {
  //     dialect: 'sqlite',
  //     pool: {
  //       max: 10,
  //       min: 0,
  //       idle: 10000,
  //     },
  //     query: {
  //       raw: true,
  //     },
  //   });

  const sequelize = setupDatabase(config, dbId);

  // await sequelize.authenticate();

  // if (config.setup) {
  //   await sequelize.sync({ force: true });
  // }

  const Errors = setupError(null, sequelize);

  return { sequelize, Errors };
};
