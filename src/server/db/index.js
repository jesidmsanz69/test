// const chalk = require('chalk');
const db = require('./models');
const dbApp = require('./models/appIT49');
// const dbOracle = require('./models/oracle');
const { config, configAppIT49 } = require('./config');

function handleFatalError(err) {
  // console.error(`${chalk.red('[FATAL ERROR]')} ${err.message}`);
  console.error(`[FATAL ERROR] ${err.message}`);
  console.error(err.stack);
  // process.exit(1);
}
//dbId 1- postgres, dbId 2 - oracle
function setup(dbId = 1) {
  switch (dbId) {
    case 2:
      // console.log('dbId', dbId);
      // console.log('configOracle', configOracle);
      return dbApp(configAppIT49, dbId).catch(handleFatalError);
    // break;
    default:
      return db(config).catch(handleFatalError);
  }
}

module.exports = setup;
