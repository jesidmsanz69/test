import fs from 'fs';

require('dotenv').config();

// eslint-disable-next-line consistent-return
const getManifest = () => {
  try {
    if (process.env.NODE_ENV !== 'development') {
      return JSON.parse(fs.readFileSync(`${__dirname}/public/manifest.json`, 'utf8'));
    }
  } catch (error) {
    console.log(`[getManifest ERROR]${error}`);
  }
};

export default getManifest;
