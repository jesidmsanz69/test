module.exports = {
  apps: [
    {
      name: 'abstract1330-com',
      script: './src/server/index.js',
      watch: ['./src/server', './src/frontend'],
      // watch: ['./src/frontend'],
      // Delay between restart
      watch_delay: 1000,
      ignore_watch: ['node_modules', './src/server/db'],
      env: {
        PORT: 3010,
        NODE_ENV: 'production',
      },
    },
  ],
};
